﻿#include <iostream>
#include <string>

using namespace std;

class Stack
{
private:
	int x;
	int s = 5;
	int* arr = new int[s];

public:
	int size()
	{
		return x;
	}
	Stack() : x(0)
	{}
	void push(int var)
	{
		x++;
		arr[x] = var;
	}
	int pop()
	{
		int var = arr[x];
		x--;
		return var;
	}
	~Stack()
	{
		if (x > 0)
			delete[] arr;
	}
};

void main()
{
	Stack a;
	a.size();
	cout << a.size() << "\t" << "Stack size: EMPTY" << endl;
	a.push(5);
	a.push(10);
	a.push(25);
	a.push(35);
	cout << a.size() << "\t" << "Stack size: NUMBER" << endl;
	cout << a.pop() << "\t" << "Top element" << endl;
}